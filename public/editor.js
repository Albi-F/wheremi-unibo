import * as map from "./js modules/map.js";
import * as recorder from "./js modules/microphone.js";

var wholePopup = document.querySelector("#wholePopup");
var startRecordingPopup = wholePopup.querySelectorAll(".insidePopup")[0];
var stopRecordingPopup = wholePopup.querySelectorAll(".insidePopup")[1];
var saveRecordingPopup = wholePopup.querySelectorAll(".insidePopup")[2];

var recordingStateMachine = {
  state: {
    status: null,
    get value() {
      return this.status;
    },
    set value(val) {
      this.status = val;
      this.listener(val);
    },
    listener: function(state) {
      switch(state) {
        case recordingStateMachine.states.START_RECORDING:
          if (!saveRecordingPopup.hasAttribute("hidden")) {
            saveRecordingPopup.setAttribute("hidden", "");
          };
          startRecordingPopup.removeAttribute("hidden");
          map.marker.getPopup().update();
          map.markerMovementAllowed.value = true;
          break;
        case recordingStateMachine.states.STOP_RECORDING:
          startRecordingPopup.setAttribute("hidden", "");
          stopRecordingPopup.removeAttribute("hidden");
          map.marker.getPopup().update();
          map.markerMovementAllowed.value = false;
          break;
        case recordingStateMachine.states.SAVE_RECORDING:
          stopRecordingPopup.setAttribute("hidden", "");
          saveRecordingPopup.removeAttribute("hidden");
          map.marker.getPopup().update();
          break;
      }
    }
  },
  states: {
    START_RECORDING: 'start',
    STOP_RECORDING: 'stop',
    SAVE_RECORDING: 'save'
  }
}

function delay(timer) {
    return new Promise(resolve => {
        timer = timer || 2000;
        setTimeout(function () {
            resolve();
        }, timer);
    });
};

var mediaRecorder = null;
var oldAudioURL;
(async () => {
    mediaRecorder = await recorder.mediaRecorderPrompt();
    startRecordingPopup.addEventListener("click", () => {
      mediaRecorder.start();
      recordingStateMachine.state.value = recordingStateMachine.states.STOP_RECORDING;
    });
    stopRecordingPopup.addEventListener("click", async () => {
      mediaRecorder.stop();
      while (!recorder.audioURL || recorder.audioURL === oldAudioURL) {
        await delay(200);
      }
      document.querySelector("#audioControls").src = recorder.audioURL;
      oldAudioURL = recorder.audioURL;
      recordingStateMachine.state.value = recordingStateMachine.states.SAVE_RECORDING;
    });
    var newClipButton = document.querySelector("#newClip");
    newClipButton.addEventListener("click", () => {
      recordingStateMachine.state.value = recordingStateMachine.states.START_RECORDING;
    })
})();

var mapLoaded = map.initMap();      // Initialize the map
if (mapLoaded) {
  var popup = document.querySelector("#recordingPopup");
  popup.appendChild(wholePopup);
  var popupWrapper = document.querySelector(".leaflet-popup-content-wrapper");
  recordingStateMachine.state.value = recordingStateMachine.states.START_RECORDING;
};

const App = new Vue({
  el: "#vueData",
  data: {
    titolo: "",
    purpose: ""
  }
});
