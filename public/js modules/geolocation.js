// Variable used to indicate the user that the position is being retrived
export var retrivingPosition = {
  status: false,
  get value() {
    return this.status;
  },
  set value(val) {
    this.status = val;
    this.registeredListener(val);
  },
  addListener: function(listener) {
    this.registeredListener = listener;
  },
  registeredListener: function() {}
};

export function getLocation(successfullCallback) {
  // Checking if geolocation is available in the current browser
  if ("geolocation" in navigator) {
    retrivingPosition.value = true;
    navigator.geolocation.getCurrentPosition(function (position) {
      geo_success(position, successfullCallback);
    }, geo_error, geo_options);
  }
  else {
    alert("Error! It hasn't been possible to read your actual position beacause your browser doesn't support geolocation");
  }
}

function geo_success(position, callback) {
  retrivingPosition.value = false;
  callback(position);
}

function geo_error(error) {
  retrivingPosition.value = false;
  console.log("Sorry, no position available." + '\n' + error.code + ': ' + error.message);
  if (error.code === 1) {
    alert("You've denied the authorization to read your position, \ncheck your browser settings.");
  }
  else {
    alert("Error! Try again");
  }
}

var geo_options = {
  enableHighAccuracy: true,
  //  Maximum age in milliseconds of a possible cached position that is acceptable to return
  maximumAge        : 30000,
  // Maximum length of time (in milliseconds) the device is allowed to take in order to return a position
  timeout           : 9000
};
