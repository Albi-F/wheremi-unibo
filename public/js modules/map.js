import * as geolocation from "./geolocation.js";

export var map;
export var tileLayer;
export var marker;
export var popup;
export var markerMovementAllowed = {
	state: true,
	set value(val) {
		this.state = val;
	}
}

// This function is used to create the map
export function initMap() {
	// set up the map
	map = new L.Map('map', {
    // Italy
    center: [41.8719, 12.5674],
    zoom: 6,
    // This prevents popup from closing
    closePopupOnClick: false
  });

	// create the tile layer with correct attribution
	var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmAttrib='Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
	tileLayer = new L.TileLayer(osmUrl, {minZoom: 2, maxZoom: 19, attribution: osmAttrib});
	map.addLayer(tileLayer);

  // create the popup
  popup = document.createElement('div');
  popup.id = "recordingPopup";

	// create the marker
  marker = L.marker([41.8719, 12.5674], {
    alt: "You are here",
    interactive: false,
  }).addTo(map)
    .bindPopup(popup, {
			maxWidth: "auto"
		})
    .openPopup();

		// create the button to center the map on the position of the user
  var retrievePositionControl = L.Control.extend({
    options: {
      position: "bottomright"
    },
    onAdd: function (map) {
      var container = L.DomUtil.create("div", "leaflet-bar leaflet-control leaflet-control-custom");
			container.id = "searchButton";
			container.title = "Find my position"
      L.DomEvent.disableClickPropagation(container);
      L.DomEvent.on(container, "click", function(ev) {
				if (markerMovementAllowed.state) {
        	geolocation.getLocation(updateMapLocation);
				} else {
					alert("Non è possibile scegliere una nuova posizione durante la registrazione o il saltaggio di una clip audio");
				}
      });
      return container;
    }
  });
  map.addControl(new retrievePositionControl());

	// this is to make the user aware that I'm waiting for his position
	geolocation.retrivingPosition.addListener(function(status) {
		let mapDOM = document.querySelector("#map");
		if (status == true) {
			mapDOM.style.cursor = "progress";
		} else {
			mapDOM.style.cursor = "default";
		}
	});

	// create the search bar
	map.addControl(new L.Control.Search({
			url: 'https://nominatim.openstreetmap.org/search?format=json&q={s}',
			jsonpParam: 'json_callback',
			propertyName: 'display_name',
			propertyLoc: ['lat','lon'],
			marker: marker,
			autoCollapse: false,
			autoType: false,
			minLength: 2
	}));

	// clicking on the map will move the marker and center the map in that place
  map.on("click", function(ev) {
		if (markerMovementAllowed.state) {
    	map.setView(ev.latlng,map.getZoom());
    	marker.setLatLng(ev.latlng);
		}
	})

	return true;
}

// This function is used to update the place showed by the map
function updateMapLocation(position) {
  var position = new L.LatLng(position.coords.latitude, position.coords.longitude);
  map.setView(position,18);
  marker.setLatLng(position);
}
