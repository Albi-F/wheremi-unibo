"use strict";

const express = require("express");
const app = express();

const https = require("https");

const fs = require("fs");

const port = 3000

//-------------------- APP -----------------------

var auxiliaryListener = https.createServer({
	key: fs.readFileSync(__dirname + "/security/server.key", "utf8"),
	cert: fs.readFileSync(__dirname + "/security/server.cert", "utf8")
}, app).listen(port, function () {
    console.log("Node listening on https://localhost:" + auxiliaryListener.address().port);
    console.log("Geolocation Test!");
})

app.use(express.static(__dirname + "/public"));

app.get("/", (req, res) => {
	res.redirect("geolocation.html");
});
